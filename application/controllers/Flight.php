<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Flight extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('date');
        $this->load->helper(array('form'));
        $this->load->model('Flight_model');
        $this->_init();
    }

    private function _init() {
        $this->output->set_template('custom_tpl');
    }

    public function index() {
        $data['flight_list'] = $this->Flight_model->get_all_data();
        $this->load->view('flight/view_flight', $data);
    }

    public function insert_new_data() {
        $cdata['flight_id'] = uniqid('', true);
        $cdata['flight_number'] = $this->input->post('flight_number');
        $cdata['origin'] = $this->input->post('origin');
        $cdata['destination'] = $this->input->post('destination');
        $cdata['carrier'] = $this->input->post('carrier');
        $cdata['price'] = $this->input->post('price');
        $cdata['day'] = $this->input->post('day');
        $cdata['time'] = $this->input->post('time');
        $cdata['duration'] = $this->input->post('duration');
        $cdata['available_seats'] = $this->input->post('available_seats');
        $cdata['created_at'] = date("Y-m-d H:i:s");
        $res = $this->Flight_model->insert_data_to_db($cdata);
        if ($res) {
            $data['flight_list'] = $this->Flight_model->get_all_data();
            $this->load->view('flight/view_flight', $data);
        }
    }

    public function update() {
        $flight_id = $this->input->post('flight_id');
        $cdata['flight_number'] = $this->input->post('flight_number');
        $cdata['origin'] = $this->input->post('origin');
        $cdata['destination'] = $this->input->post('destination');
        $cdata['carrier'] = $this->input->post('carrier');
        $cdata['price'] = $this->input->post('price');
        $cdata['day'] = $this->input->post('day');
        $cdata['time'] = $this->input->post('time');
        $cdata['duration'] = $this->input->post('duration');
        $cdata['available_seats'] = $this->input->post('available_seats');
        $cdata['created_at'] = date("Y-m-d H:i:s");
        $res = $this->Flight_model->update_data($cdata,$flight_id);
        if ($res) {
            $data['flight_list'] = $this->Flight_model->get_all_data();
            $this->load->view('flight/view_flight', $data);
        }
    }

    public function delete($id) {
        $this->Flight_model->delete_data($id);
        $this->index();
    }

    function do_upload() {
        $config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"]) . '/assets/upload/';
        $config['upload_url'] = base_url() . 'assets/upload/';
        $config['allowed_types'] = '*';
        $config['overwrite'] = TRUE;
        $config['max_size'] = '1000KB';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('flight/send_error', $error);
        } else {
            $data = array('upload_data' => $this->upload->data());
            if ($this->upload->data()) {
                $n = 1;
                foreach ($this->upload->data() as $value) {
                    if ($n == 4) {
                        $path = $value;
                    }
                    $n++;
                }
            }

            if (isset($path)) {
                $tab = "^";
                $fp = fopen($path, 'r');
                $n = 0;
                while (!feof($fp)) {
                    $line = fgets($fp, 2048);
                    $data['list'] = str_getcsv($line, $tab);
                    $data_arr[$n] = array(
                        'flight_number' => $data['list'] [0],
                        'origin' => $data['list'] [1],
                        'destination' => $data['list'] [2],
                        'carrier' => $data['list'] [3],
                        'price' => $data['list'] [4],
                        'day' => $data['list'] [5],
                        'time' => $this->check_time($data['list'] [6]),
                        'duration' => $data['list'] [7],
                        'available_seats' => $data['list'] [8],
                        'created_at' => date("Y-m-d H:i:s")
                    );
                    if ($this->Flight_model->check_redudant_data('flights', array('flight_number' => $data['list'] [0]))) {
                        $n++;
                    }
                    //print_r($data['list']);
                    //exit;
                }
                fclose($fp);
            }
            $res = $this->Flight_model->insert_data_array_to_db($data_arr);
            if ($res) {
                redirect('/', 'location', 301);
            } else {
                redirect('/', 'location', 301);
            }
        }
    }

    public function check_time($time) {
        list($hours, $minutes) = explode(':', $time);
        $startTimestamp = mktime($hours, $minutes);
        return date('H:i', $startTimestamp);
    }

}
