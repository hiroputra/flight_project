<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Book_ticket extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('date');
        $this->load->helper(array('form'));
        $this->load->model('Flight_model');
        $this->load->model('Book_ticket_model');
        $this->_init();
    }

    private function _init() {
        $this->output->set_template('custom_tpl');
    }

    public function index() {
        $data['origin_list'] = $this->Flight_model->get_origin_flight();
        $data['dest_list'] = $this->Flight_model->get_dest_flight();
        $data['seats_remain'] = $this->Flight_model->get_available_seat_flight();
        $data['book_list'] = $this->Book_ticket_model->get_all_data();
        $this->load->view('book_ticket/view_book_ticket', $data);
    }

    public function insert_new_data() {
        //$cdata['book_ticket_id'] = uniqid('', true);
        $cdata['name'] = $this->input->post('user_name');
        $cdata['origin'] = $this->input->post('origin');
        $cdata['destination'] = $this->input->post('destination');
        $cdata['flight_number'] = $this->input->post('flight_number');
        $cdata['seat'] = $this->input->post('seat');
        $cdata['created_at'] = date("Y-m-d H:i:s");
        $res = $this->Book_ticket_model->insert_data_to_db($cdata);
        if ($res) {
            $data['book_list'] = $this->Book_ticket_model->get_all_data();
            $this->load->view('book_ticket/view_book_ticket', $data);
        }
    }

    public function edit() {
        $id = $this->uri->segment(3);
        $data['book_detail'] = $this->Book_ticket_model->get_data_by_id($id);
        $data['origin_list'] = $this->Flight_model->get_origin_flight();
        $data['dest_list'] = $this->Flight_model->get_dest_flight();
        $data['seats_remain'] = $this->Flight_model->get_available_seat_flight();
        $data['book_list'] = $this->Book_ticket_model->get_all_data();
        $this->load->view('book_ticket/view_book_ticket', $data);
    }

    public function update() {
        $book_ticket_id = $this->input->post('book_ticket_id');
        $cdata['name'] = $this->input->post('user_name');
        $cdata['origin'] = $this->input->post('origin');
        $cdata['destination'] = $this->input->post('destination');
        $cdata['flight_number'] = $this->input->post('flight_number');
        $cdata['seat'] = $this->input->post('seat');
        $cdata['created_at'] = date("Y-m-d H:i:s");
        $res = $this->Book_ticket_model->update_data($cdata, $book_ticket_id);
        if ($res) {
            $data['origin_list'] = $this->Flight_model->get_origin_flight();
            $data['dest_list'] = $this->Flight_model->get_dest_flight();
            $data['seats_remain'] = $this->Flight_model->get_available_seat_flight();
            $data['book_list'] = $this->Book_ticket_model->get_all_data();
            $this->load->view('book_ticket/view_book_ticket', $data);
        } else {
            $data['origin_list'] = $this->Flight_model->get_origin_flight();
            $data['dest_list'] = $this->Flight_model->get_dest_flight();
            $data['seats_remain'] = $this->Flight_model->get_available_seat_flight();
            $data['book_list'] = $this->Book_ticket_model->get_all_data();
            $this->load->view('book_ticket/view_book_ticket', $data);
        }
    }

    public function delete($id) {
        $this->Book_ticket_model->delete_data($id);
        $this->index();
    }

    function do_upload() {
        $config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"]) . '/assets/upload/';
        $config['upload_url'] = base_url() . 'assets/upload/';
        $config['allowed_types'] = '*';
        $config['overwrite'] = TRUE;
        $config['max_size'] = '1000KB';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('flight/send_error', $error);
        } else {
            $data = array('upload_data' => $this->upload->data());
            if ($this->upload->data()) {
                $n = 1;
                foreach ($this->upload->data() as $value) {
                    if ($n == 4) {
                        $path = $value;
                    }
                    $n++;
                }
            }

            if (isset($path)) {
                $tab = "^";
                $fp = fopen($path, 'r');
                $n = 0;
                while (!feof($fp)) {
                    $line = fgets($fp, 2048);
                    $data['list'] = str_getcsv($line, $tab);
                    $data_arr[$n] = array(
                        'flight_number' => $data['list'] [0],
                        'origin' => $data['list'] [1],
                        'destination' => $data['list'] [2],
                        'carrier' => $data['list'] [3],
                        'price' => $data['list'] [4],
                        'day' => $data['list'] [5],
                        'time' => $this->check_time($data['list'] [6]),
                        'duration' => $data['list'] [7],
                        'available_seats' => $data['list'] [8],
                        'created_at' => date("Y-m-d H:i:s")
                    );
                    if ($this->FlightModel->check_redudant_data('flights', array('flight_number' => $data['list'] [0]))) {
                        $n++;
                    }
                    //print_r($data['list']);
                    //exit;
                }
                fclose($fp);
            }
            $res = $this->FlightModel->insert_flight_array_to_db($data_arr);
            if ($res) {
                redirect('/', 'location', 301);
            } else {
                redirect('/', 'location', 301);
            }
        }
    }

    public function check_time($time) {
        list($hours, $minutes) = explode(':', $time);
        $startTimestamp = mktime($hours, $minutes);
        return date('H:i', $startTimestamp);
    }

}
