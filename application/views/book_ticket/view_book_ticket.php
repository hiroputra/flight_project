<?php
if (isset($book_detail)) {
    foreach ($book_detail as $datas) {
        $id = $datas->book_ticket_id;
        $flight_number = $datas->flight_number;
        $name = $datas->name;
        $origin = $datas->origin;
        $adestination = $datas->destination;
        $seat = $datas->seat;
    }
}
if (isset($id)) {
    $action_url = base_url() . 'Book_ticket/update';
    $btn_caption = 'update';
} else {
    $action_url = base_url() . 'Book_ticket/insert_new_data';
    $btn_caption = 'Submit';
}
?>
<div class="top-content">
    <div class="inner-bg">
        <div class="container">

            <div class="row">
                <div class="col-sm-8">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Book Ticket Flight </h3>

                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-lock"></i>
                        </div>
                    </div>

                    <div class="form-bottom">
                        <form role="form" method="post" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo $action_url; ?>">
                            <input type="hidden" name="book_ticket_id" value="<?= isset($id) ? $id : ''; ?>" >
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-id">name:</label>
                                <div class="col-sm-10">    
                                    <input type="text" name="user_name" placeholder="user name" class="form-username form-control" id="form-username" value="<?= isset($name) ? $name : ''; ?>" >
                                </div>     
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="origin-list">Origin:</label>
                                <div class="col-sm-10">
                                    <select name="origin" class="form-control" id="origint-list">
                                        <?php
                                        foreach ($origin_list as $data) {
                                            ?>                                            
                                        <option value="<?= $data->origin ?>" <?= isset($origin) ? (($origin == $data->origin) ? "selected" : "") : "" ?>><?= $data->origin ?></option>
                                            </tr>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="destination-list">Destination:</label>
                                <div class="col-sm-10">
                                    <select name="destination" class="form-control" id="destination-list">
                                        <?php
                                        foreach ($dest_list as $data) {
                                            ?>
                                            <option value="<?= isset($destination) ? $destination : $data->destination ?>"><?= isset($destination) ? $destination : $data->destination ?></option>
                                            </tr>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-id">Seats:</label>
                                <div class="col-sm-10">    
                                    <input type="text" name="seat" placeholder="seat" class="form-seat form-control" id="form-seat" value="<?= isset($seat) ? $seat : ''; ?>" >
                                </div>     
                            </div>       

                            <input type="hidden" name="flight_name" placeholder="flight_name" class="form-flight_name form-control" id="form-username" >
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-id"></label>
                                <div class="col-sm-10">   
                                    <button type="submit" name="submit" value="Save" class="btn"><?= $btn_caption ?></button>
                                </div>     
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</br></br>
<table class="table">
    <tr style="background-color:#0080C0;">
        <th style="color:#fff;">Book List</th>      
    </tr>
</table>

<table class="table table-bordered">
    <thead style="background-color:#f5f5f5;">
        <tr style="color:#808080;">
            <!--<th>Book Id</th>-->
            <th>Name</th>
            <th>Flight</th>
            <th>Origin</th>
            <th>Destination</th>
            <th>seat</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th scope="col" colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>

        <?php
        foreach ($book_list as $data) {
            ?>
            <tr style="color:#808080;">
                 <!--<td><?php echo $data->book_ticket_id; ?></td>-->
                <td><?php echo $data->name; ?></td>
                <td><?php echo $data->flight_number; ?></td>
                <td><?php echo $data->origin; ?></td>
                <td><?php echo $data->destination; ?></td>
                <td><?php echo $data->seat; ?></td>                
                <td><?php echo $data->created_at; ?></td>
                <td><?php echo $data->updated_at; ?></td>                
                <td width="40" align="left" ><a href="#" onClick="show_confirm('edit',<?php echo $data->book_ticket_id; ?>)">Edit</a></td>
                <td width="40" align="left" ><a href="#" onClick="show_confirm('delete',<?php echo $data->book_ticket_id; ?>)">Delete </a></td>                 
            </tr>
        <?php } ?>
    </tbody>
</table>   


<script type="text/javascript">
    function show_confirm(act, gotoid)
    {
        if (act == "edit") {
            var r = confirm("Do you really want to edit?");
        } else if (act == "delete") {
            var r = confirm("Do you really want to delete?");
        } else if (act == "create_user") {
            var r = confirm("Do you want to create_user?");
        }
        if (r == true)
        {
            window.location = "<?php echo base_url(); ?>book_ticket/" + act + "/" + gotoid;
        }
    }
</script>

