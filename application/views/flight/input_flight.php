
<div class="top-content">
    <div class="inner-bg">
        <div class="container">

            <div class="row">
                <div class="col-sm-8">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Input Flight </h3>

                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-lock"></i>
                        </div>
                    </div>

                    <div class="form-bottom">
                        <form role="form" method="post" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url(); ?>customerCn/insert_new_customer">
                            
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-id">name:</label>
                                <div class="col-sm-10">    
                                    <input type="text" name="customer_name" placeholder="name" class="form-username form-control" id="form-username" >
                                </div>     
                            </div>
      
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="origin-list">Origin:</label>
                                <div class="col-sm-10">
                                    <select name="origin" class="form-control" id="origint-list">
                                        <?php
                                        foreach ($origin_list as $data) {
                                            ?>
                                            <option value="<?= $data->origin; ?>"><?= $data->origin; ?></option>
                                            </tr>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="destination-list">Destination:</label>
                                <div class="col-sm-10">
                                    <select name="destination" class="form-control" id="destination-list">
                                        <?php
                                        foreach ($destination_list as $data) {
                                            ?>
                                            <option value="<?= $data->destination; ?>"><?= $data->destination; ?></option>
                                            </tr>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-id">Seats:</label>
                                <div class="col-sm-10">    
                                    <input type="text" name="seat" placeholder="seat" class="form-seat form-control" id="form-seat" >
                                </div>     
                            </div>                         
                            
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-id"></label>
                                <div class="col-sm-10">   
                                    <button type="submit" name="submit" value="Update" class="btn">Save</button>
                                </div>     
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
