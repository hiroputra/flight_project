<div class="pull-right">            
    <button type="button" name="btn_import_file" class="btn btn-success" data-toggle="modal" data-target="#AttachFile">Upload File</button>
</div>
</br></br>
<table class="table">
    <tr style="background-color:#0080C0;">
        <th style="color:#fff;">Flights List</th>      
    </tr>
</table>

<table class="table table-bordered">
    <thead style="background-color:#f5f5f5;">
        <tr style="color:#808080;">
            <th>Flight Number</th>
            <th>Origin Airport</th>
            <th>Destination Airport</th>
            <th>Carrier</th>
            <th>Price</th>
            <th>Day</th>
            <th>Time</th>
            <th>Duration</th> 
            <th>Available Seats</th>             
            <th scope="col" colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>

        <?php
        foreach ($flight_list as $data) {
            ?>
            <tr style="color:#808080;">
                <td><?php echo $data->flight_number; ?></td>
                <td><?php echo $data->origin; ?></td>
                <td><?php echo $data->destination; ?></td>
                <td><?php echo $data->carrier; ?></td>
                <td><?php echo $data->price; ?></td>
                <td><?php echo $data->day; ?></td>
                <td><?php echo date('H:i', strtotime($data->time));?></td> 
                <td><?php echo $data->duration; ?></td>
                <td><?php echo $data->available_seats; ?></td>                              
                <td width="40" align="left" ><a href="#" onClick="show_confirm('edit',<?php echo $data->flight_id; ?>)">Edit</a></td>
                <td width="40" align="left" ><a href="#" onClick="show_confirm('delete',<?php echo $data->flight_id; ?>)">Delete </a></td> 
            </tr>
        <?php } ?>
    </tbody>
</table>   

<div class="modal fade" id="AttachFile" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Upload File</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-10">
                        <?php echo form_open_multipart('flight/do_upload'); ?>
                        <input type="file" name="userfile" size="20" />
                    </div>
                </div> 
                <div class="form-group">
                    <div class="col-sm-10">
                    </div>
                </div>
            </div>            
            <div class="modal-footer">
                <input type="submit" value="upload" />
                <button type="button" class="button_gold" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function show_confirm(act, gotoid)
    {
        if (act == "edit") {
            var r = confirm("Do you really want to edit?");
        } else if (act == "delete") {
            var r = confirm("Do you really want to delete?");
        } else if (act == "create_user") {
            var r = confirm("Do you want to create_user?");
        }
        if (r == true)
        {
            window.location = "<?php echo base_url(); ?>Flight/" + act + "/" + gotoid;
        }
    }
    var upl_url = '';
    function import_file(url) {
        window.upl_url = url;
        $('#AttachFile').modal('show');
    }
</script>

