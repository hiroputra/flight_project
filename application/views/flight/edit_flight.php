<?php
foreach ($customer_detail as $data) {
    $id = $data->id;
    $cust_id = $data->cust_id;
    $cif = $data->cif;
    $sid = $data->sid;
    $name = $data->name;
    $email = $data->email;
    $address = $data->address;
    $payment_type = $data->payment_type;
    $subs_bank_acc = $data->subs_bank_acc;
    $redm_bank_acc = $data->redm_bank_acc;
    $phone = $data->phone;
    $photo_path = $data->photo;
    $created_at = $data->created_at;
    $updated_at = $data->updated_at;
}
?>
<!-- Top content -->
<div class="top-content">
    <div class="inner-bg">
        <div class="container">

            <div class="row">
                <div class="col-sm-8">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Edit Customer </h3>

                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-lock"></i>
                        </div>
                    </div>

                    <div class="form-bottom">
                        <form role="form" method="post" class="col-md-20 form-horizontal" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url(); ?>customerCn/update">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-id">ID:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="id"  class="form-username form-control" id="form-username" value="<?php echo $id; ?>">
                                </div>    
                            </div>                            
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-id">name:</label>
                                <div class="col-sm-10">    
                                    <input type="text" name="customer_name" placeholder="name" class="form-username form-control" id="form-username" value="<?php echo $name; ?>">
                                </div>     
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-email">email:</label>
                                <div class="col-sm-10">   
                                    <input type="email" name="email" placeholder="email" class="form-password form-control" id="form-email" value="<?php echo $email; ?>">
                                </div>     
                            </div>     
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-email">sid:</label>
                                <div class="col-sm-10">   
                                    <input type="text" name="sid" placeholder="SID" class="form-password form-control" id="form-email" value="<?php echo $sid; ?>">
                                </div>     
                            </div>                             
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-email">cifs:</label>
                                <div class="col-sm-10">   
                                    <input type="number" name="cif" placeholder="CIFS" class="form-password form-control" id="form-email" value="<?php echo $cif; ?>">
                                </div>     
                            </div>                              
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-email">address:</label>
                                <div class="col-sm-10">   
                                    <input type="text" name="address" placeholder="Address" class="form-password form-control" id="form-email" value="<?php echo $address; ?>">
                                </div>     
                            </div>          


                            <div class="form-group">
                                <label class="control-label col-sm-2" for="product-list">payment_type:</label>
                                <div class="col-sm-10">
                                    <select name="payment_type" class="form-control" id="product-list">
                                        <?php
                                        foreach ($payment_list as $data) {
                                            ?>
                                            <option value="<?php echo $data->id; ?>"><?php echo $data->payment_name; ?></option>
                                            </tr>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-email">subs_bank_acc:</label>
                                <div class="col-sm-10">   
                                    <input type="text" name="subs_bank_acc" placeholder="Subscriptions Bank Account" class="form-password form-control" id="form-email" value="<?php echo $subs_bank_acc; ?>">
                                </div>     
                            </div>                              
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-email">redm_bank_acc:</label>
                                <div class="col-sm-10">   
                                    <input type="text" name="redm_bank_acc" placeholder="Redemption Bank Account" class="form-password form-control" id="form-email" value="<?php echo $redm_bank_acc; ?>">
                                </div>     
                            </div>                              
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-email">phone:</label>
                                <div class="col-sm-10">   
                                    <input type="text" name="phone" placeholder="Phone Number" class="form-password form-control" id="form-email" value="<?php echo $phone; ?>">
                                </div>     
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-ihsg">Photo :</label>      
                                <div class="col-md-2">
                                    <img src="<?php echo $photo_path; ?>" alt="" class="img-responsive img-thumbnail" />
           
                                    <input type="file" name="userfile" size="20" />
                                </div>
                                            
                            </div>  
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="form-id"></label>
                                <div class="col-sm-10">   
                                    <button type="submit" name="submit" value="Update" class="btn">Save</button>
                                </div>     
                            </div> 

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
