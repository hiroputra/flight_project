<!-- /.modal compose message -->
    <div class="modal show" id="modalError" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true" data-backdrop="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <form role="form" class="form-horizontal">
                <div class="form-group">
                    <?php echo $error; ?>                  
                </div>
            </form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal compose message -->