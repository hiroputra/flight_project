<!DOCTYPE html>
<html lang="en">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <!----Header--->
            <title>Flight Status</title>            
            <link href="<?php echo base_url(); ?>assets/themes/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
            <link href="<?php echo base_url(); ?>assets/themes/bootstrap/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">              
            <link href="<?php echo base_url(); ?>assets/themes/default/css/nki_style.css" rel="stylesheet" type="text/css">

            <script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery.min.js"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/default/js/jquery-1.11.3.min.js"></script>     
            <script type="text/javascript" src="<?php echo base_url(); ?>assets/themes/bootstrap/js/bootstrap.min.js"></script>  
            
            <script>
                $(document).ready(function() {
                    var navpos = $('#mainnav').offset();
                    console.log(navpos.top);
                    $(window).bind('scroll', function() {
                        if ($(window).scrollTop() > navpos.top) {
                            $('#mainnav').addClass('navbar navbar-fixed-top navbar-custom');
                            $('#langnav').removeClass('nav navbar-nav navbar-right set-align-to-right');
                            $('#langnav').addClass('nav navbar-nav navbar-right');
                        }
                        else {

                            $('#mainnav').removeClass('navbar navbar-fixed-top navbar-custom');
                            $('#mainnav').addClass('navbar navbar-static-top navbar-custom');
                            $('#langnav').removeClass('nav navbar-nav navbar-right');
                            $('#langnav').addClass('nav navbar-nav navbar-right set-align-to-right');
                        }
                    });
                });
            </script> 
            </head>
            <body class="custom_body1">
                <div class="logo_area">    
                    <div class="left_logo">
                        <img src="<?php echo base_url(); ?>assets/themes/default/images/Departures_blue.png" alt="logo" style="height:57px; width:167px;"/>
                    </div>
                </div>
                <!-- Navigation -->

                  <nav id="mainnav" class="fixed navbar navbar-static-top navbar-custom" role="navigation" >  
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>                        
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="navbar-mid-collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="<?php echo site_url('Flight'); ?>" />Home</a></li>
                                <li><a href="<?php echo site_url('Book_ticket'); ?>" />Booking</a></li>                                
                            </ul>                                
                        </div>      
                        
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container -->
                </nav>

                <!-- Page Content -->
                <div class="container">
                    <?php if ($this->load->get_section('text_header') != '') { ?>
                        <h1><?php echo $this->load->get_section('text_header'); ?></h1>
                    <?php } ?>
                    <?php echo $output; ?>
                    <hr>
                </div> 

                        <footer class="footer">
                            <div class="navbar navbar-custom custom-footer ">
                                <div class="container">
                                    <div class="navbar-collapse collapse" id="footer-body">
                                        <ul class="nav navbar-nav navbar-left"> 
                                            <li style="padding-top:15px; ">Copyright &copy; 2017 Hiroputra</li>
                                        </ul>                         
                                    </div>
                                </div>
                            </div>
                        </footer>
                    </body>
    </html>