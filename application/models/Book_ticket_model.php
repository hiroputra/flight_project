<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Book_ticket_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_all_data() {
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get('book_tickets');
        return $query->result();
    }

    public function insert_data_to_db($data) {
        return $this->db->insert('book_tickets', $data);
    }

    public function get_data_by_id($id) {
        if ($id) {
            $this->db->where('book_ticket_id', $id);
            return $this->db->get('book_tickets')->result();
        } else {
            return false;
        }
    }

    public function update_data($data, $id) {
        $this->db->where('book_ticket_id', $id);
        return $this->db->update('book_tickets', $data);
    }

    public function delete_data($id) {
        $this->db->where('book_ticket_id', $id);
        return $this->db->delete('book_tickets');
    }

    public function check_redudant_data($table = false, $where = false) {
        if ($table && $where) {
            return ($this->db->where($where)->get($table)->num_rows() == 0);
        }
        return false;
    }

}
