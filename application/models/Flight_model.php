<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Flight_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_all_data() {
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get('flights');
        return $query->result();
    }

    public function insert_data_to_db($data) {
        return $this->db->insert('flights', $data);
    }

    public function insert_data_array_to_db($data = false) {
        $this->db->trans_start();
        $this->db->insert_batch('flights', $data);
        if (!$this->db->affected_rows()) {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return $this->db->insert_id();
    }

    public function check_redudant_data($table = false, $where = false) {
        if ($table && $where) {
            return ($this->db->where($where)->get($table)->num_rows() == 0);
        }
        return false;
    }

    public function update_data($data, $id) {
        $this->db->where('flight_id', $id);
        return $this->db->update('flights', $data);
    }

    public function delete_data($id) {
        $this->db->where('flight_id', $id);
        return $this->db->delete('flights');
    }

    public function get_origin_flight() {
        $res = $this->db->query("SELECT DISTINCT origin FROM flights")->result();
        return $res;
    }

    public function get_dest_flight() {
        $res = $this->db->query("SELECT DISTINCT destination FROM flights")->result();
        return $res;
    }

    public function get_available_seat_flightss() {
        $this->db->order_by('id');
        $query = $this->db->get('flights');
        return $query->result();
    }

    public function get_available_seat_flight($origin=false,$destination=false) {
        $this->db->select('available_seats')
                ->where('origin', $origin)
                ->where('destination', $destination);
        $res = $this->db->get('flights')->result();
    }

}
